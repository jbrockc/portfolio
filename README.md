## Simple Portfolio

The following sites use the Ananda Designs plugin which I worked on. The plugin is unique in that it sits in between the parent theme (X-theme) and the child theme. I didn't choose this approach but simply worked on the implementation of it. More than 30 sites are either planning to use this plugin eventually or are currently making the transition. 

1. https://meditationretreat.org
2. https://www.onlinewithananda.org/
3. https://www.anandavillage.org/
4. https://www.expandinglight.org/
5. https://anandayogaretreat.org/
6. https://www.crystalclarity.com/
